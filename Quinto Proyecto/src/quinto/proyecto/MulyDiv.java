package quinto.proyecto;

import java.util.Scanner;

public class MulyDiv {

    public int num1;
    public int num2;
    Scanner data = new Scanner(System.in);

    public MulyDiv(int n1, int n2) {
        num1 = n1;
        num2 = n2;

    }

    public int getNum1() {
        System.out.println("Ingrese el primer numero:");
        num1 = data.nextInt();
        return num1;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public int getNum2() {
        System.out.println("Ingrese el segundo numero:");
        num2 = data.nextInt();
        return num2;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }

}
