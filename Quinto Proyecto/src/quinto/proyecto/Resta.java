
package quinto.proyecto;

public class Resta extends MulyDiv{
    private int res;
    public Resta(int n1, int n2, int res) {
        super(n1, n2);
        this.res=res;
    }
      public int getRes() {
        res=num1-num2;
        return res;
    }

    public void setSum(int res) {
        this.res = res;
    }
    @Override
    public String toString() {
        String p = (" PRIMER NUMERO: " + getNum1() + "\n SEGUNDO NUMERO: " + getNum2()+ "\n TOTAL DE LA RESTA "+getRes());
        System.out.println("--------------------------------------------------------");
        return p;

    }
}
