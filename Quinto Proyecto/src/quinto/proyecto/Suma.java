
package quinto.proyecto;


public class Suma extends MulyDiv{
    private int sum;
    public Suma(int n1, int n2, int sum) {
        super(n1, n2);
        this.sum=sum;
    }

    public int getSum() {
        sum=num1+num2;
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
    @Override
    public String toString() {
        String p = (" PRIMER NUMERO: " + getNum1() + "\n SEGUNDO NUMERO: " + getNum2()+ "\n TOTAL DE LA SUMA "+getSum());
        System.out.println("--------------------------------------------------------");
        return p;

    }
}
