package quinto.proyecto;

public class Multiplicacion extends MulyDiv {

    public int tmult;
    public int div;

    public Multiplicacion(int n1, int n2) {
        super(n1, n2);
        this.tmult = tmult;
        this.div=div;

    }

    public int getMult1() {
        int mm = num1 * num2;
        return mm;
    }

    public void setMult1(int mult1) {
        this.tmult = tmult;
    }

    public int getDiv() {
        double dd= num1 /num2;
        return div;
    }

    public void setDiv(int div) {
        this.div = div;
    }

    @Override
    public String toString() {
        String p = (" PRIMER NUMERO: " + getNum1() + "\n SEGUNDO NUMERO: " + getNum2()+ "\n TOTAL DE LA MULTIPLICACION :"+getMult1()+ "\n TOTAL DE LA DIVISION : "+getDiv());
        System.out.println("--------------------------------------------------------");
        return p;

    }
}
